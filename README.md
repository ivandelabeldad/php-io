# PHP Files

[![Packagist](https://img.shields.io/packagist/v/ivandelabeldad/files.svg)](https://packagist.org/packages/ivandelabeldad/dates)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Basic Files library for PHP


## Install
```
composer require ivandelabeldad/files
```


## Usage

### Creating and deleting Files
```php

$file = new File("location/file.txt");

$file->createIfNotExists();

$file->delete();
```

### Working with text files
```php
$file = new FileText("location/file.txt");

$file->writeLine("First line.");
$file->writeLine("Second line.");

// First line.
echo $file->readFirstLine();

// Second line.
echo $file->readLastLine();

// First line.
// Second line.
echo $file->readContent();

// First line.
// Second line.
foreach($file->iterator() as $key => $line) {
    echo $key . ' -> ' . $line;
}
```

### Working with CSV files
```php

$file = new FileTextCSV("location/file.csv");

// Without indexes
$content = $file->readContentArray(false);

// Using indexes there are two ways

// First way
$file->setIndexes([
    "name",
    "lastname",
]);

$content = $file->readContentArray();

// Second way
$file->useFirstLineAsIndexes();
$content = $file->readContentArray();
```

## License

The API Rackian is open-sourced software licensed under
the [MIT LICENSE](https://opensource.org/licenses/MIT)
