<?php

namespace IO;

class File
{

    /** @var string */
    protected $fileName;

    /**
     * File constructor.
     * @param string $fileName
     */
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return file_exists($this->fileName);
    }

    /**
     * @return bool
     */
    public function createIfNotExists()
    {
        if (!file_exists($this->fileName)) {
            $file = fopen($this->fileName, 'w');
            fclose($file);
        } else {
            return false;
        }
        return file_exists($this->fileName);
    }

    /**
     * @return bool
     */
    public function delete()
    {
        return unlink($this->fileName);
    }

}
