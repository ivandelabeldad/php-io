<?php

namespace IO;

use Iterator;

class FileTextIterator implements Iterator
{

    /** @var string */
    private $file;
    /** @var string */
    private $currentString = '';
    /** @var int */
    private $currentPosition;

    /**
     * FileTextIterator constructor.
     * @param string $fileName
     */
    public function __construct($fileName)
    {
        $this->file = fopen($fileName, 'r');;
        $this->currentPosition = -1;
    }

    /**
     *
     */
    public function __destruct()
    {
        fclose($this->file);
    }

    /**
     * @return string
     */
    public function current()
    {
        return $this->currentString;
    }

    /**
     *
     */
    public function next()
    {
        ++$this->currentPosition;
        $this->currentString = fgets($this->file);
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->currentPosition;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return (!feof($this->file));
    }

    /**
     *
     */
    public function rewind()
    {
        fseek($this->file, 0);
        if ($this->valid()) {
            $this->next();
        }
    }

}
