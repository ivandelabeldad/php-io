<?php

namespace IO;

use RuntimeException;

class FileTextCSV extends FileText
{

    /** @var string */
    protected $delimiter;
    /** @var string[] */
    protected $indexes;
    /** @var boolean */
    protected $useFirstLineAsIndexes;

    /**
     * Filer constructor.
     * @param string $fileName
     * @param string $delimiter
     * @param string $endOfLine
     */
    public function __construct($fileName, $delimiter = ",", $endOfLine = "\n")
    {
        parent::__construct($fileName, $endOfLine);
        $this->delimiter = $delimiter;
    }

    /**
     * @param string[] $indexes
     */
    public function setIndexes($indexes)
    {
        $this->indexes = $indexes;
    }

    /**
     *
     */
    public function useFirstLineAsIndexes()
    {
        $this->useFirstLineAsIndexes = true;
        $this->indexes = $this->readFirstLineArray(false);
    }

    /**
     * Return an array of the content splitted with the separator
     * @param bool $assoc
     * @return array[]
     */
    public function readContentArray($assoc = true)
    {
        $contentString = $this->readContent();
        $arrays = explode("\n", $contentString);
        foreach ($arrays as &$array) {
            $array = explode($this->delimiter, $array);
            $array = array_map('trim', $array);
            $array = array_filter($array);
        }
        $arrays = array_filter($arrays);
        if ($assoc) {
            $arrays = array_map(function ($array) {
                return $this->arrayToAssocArray($array);
            }, $arrays);
        }
        if ($this->useFirstLineAsIndexes) {
            array_shift($arrays);
        }
        return $arrays;
    }

    /**
     * @param bool $assoc
     * @return array
     */
    public function readFirstLineArray($assoc = true)
    {
        $contentString = $this->readFirstLine();
        $array = explode($this->delimiter, $contentString);
        $array = array_map(function ($e) {
            return trim($e);
        }, $array);
        if ($assoc) $array = $this->arrayToAssocArray($array);
        return $array;
    }

    /**
     * @param bool $assoc
     * @return array
     */
    public function readLastLineArray($assoc = true)
    {
        $contentString = $this->readLastLine();
        $array = explode($this->delimiter, $contentString);
        $array = array_map(function ($e) {
            return trim($e);
        }, $array);
        if ($assoc) $array = $this->arrayToAssocArray($array);
        return $array;
    }

    /**
     * @param callable $callback
     * @return array
     */
    public function readAsObject(callable $callback)
    {
        $objects = [];
        $arrays = $this->readContentArray(false);
        foreach ($arrays as $array) {
            $objects[] = $callback($array);
        }
        return $objects;
    }

    /**
     * Append an array to file with separators
     * @param array $array
     */
    public function writeArray($array)
    {
        $this->createIfNotExists();
        $file = fopen($this->fileName, 'a+');
        fwrite($file, implode($this->delimiter, $array) . $this->endOfLine);
        fclose($file);
    }

    /**
     * @param array[] $arrays
     */
    public function writeArrays($arrays)
    {
        foreach ($arrays as $array) {
            $this->writeArray($array);
        }
    }

    /**
     * @param bool $assoc
     * @return FileTextCSVIterator
     */
    public function iterator($assoc = true)
    {
        return new FileTextCSVIterator($this->fileName, $this->delimiter, $this->endOfLine, $assoc, $this->indexes);
    }

    /**
     *
     */
    protected function initIndexes()
    {
        $this->indexes = $this->readFirstLineArray(false);
    }

    /**
     * @param array $array
     * @return array
     */
    protected function arrayToAssocArray($array)
    {
        $this->throwErrorIfNotIndexes();
        $this->throwErrorIfNotEnoughIndexes($array);
        $result = [];
        for($i = 0; $i < count($array); $i++) {
            if (isset($this->indexes[$i])) {
                $result[$this->indexes[$i]] = $array[$i];
            } else {
                $result[$i] = $array[$i];
            }
        }
        return $result;
    }

    /**
     * @throws RuntimeException
     */
    protected function throwErrorIfNotIndexes()
    {
        $notIndexes = !$this->useFirstLineAsIndexes && $this->indexes === null;
        if ($notIndexes) throw new RuntimeException("There are not indexes.");
    }

    /**
     * @param string[] $array
     * @throws RuntimeException
     */
    protected function throwErrorIfNotEnoughIndexes($array)
    {
        $notEnoughIndexes = count($array) > count($this->indexes);
        if (!$notEnoughIndexes) return;
        $message = "Insufficient indexes. Required: " . count($array) . ". ";
        $message .= "Only " . count($this->indexes) . " were passed.";
        throw new RuntimeException($message);
    }

}
