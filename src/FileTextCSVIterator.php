<?php

namespace IO;

use Iterator;
use RuntimeException;

class FileTextCSVIterator implements Iterator
{

    /** @var string */
    private $file;
    /** @var string */
    private $delimiter;
    /** @var string */
    private $endOfLine;
    /** @var boolean */
    private $assoc;
    /** @var array */
    private $indexes;
    /** @var boolean */
    private $done;
    /** @var array */
    private $currentArray;
    /** @var int */
    private $currentPosition;

    /**
     * FileTextCSVIterator constructor.
     * @param string $fileName
     * @param string $delimiter
     * @param string $endOfLine
     * @param boolean $assoc
     * @param array $indexes
     */
    public function __construct($fileName, $delimiter, $endOfLine, $assoc, $indexes)
    {
        $this->file = fopen($fileName, 'r');
        $this->delimiter = $delimiter;
        $this->endOfLine = $endOfLine;
        $this->assoc = $assoc;
        $this->indexes = $indexes;
        $this->currentArray = [];
        $this->done = false;
        $this->currentPosition = -1;
    }

    /**
     *
     */
    public function __destruct()
    {
        fclose($this->file);
    }

    /**
     * @return array
     */
    public function current()
    {
        return $this->currentArray;
    }

    /**
     *
     */
    public function next()
    {
        $nextLine = trim(fgets($this->file));
        if (strpos($nextLine, $this->delimiter)) {
            ++$this->currentPosition;
            $this->currentArray = explode($this->delimiter, $nextLine);
            $this->currentArray = array_map(function ($e) {
                return trim($e);
            }, $this->currentArray);
            $this->currentArray = $this->arrayToAssocArray($this->currentArray);
        }
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->currentPosition;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return (!feof($this->file));
    }

    /**
     *
     */
    public function rewind()
    {
        fseek($this->file, 0);
        if ($this->valid()) {
            $this->next();
        }
    }

    /**
     * @param array $array
     * @return array
     */
    private function arrayToAssocArray($array)
    {
        if (count($array) > count($this->indexes)) {
            $message = "Insufficient indexes. Required: " . count($array) . ". Only have " . count($this->indexes);
            throw new \RuntimeException($message);
        }
        $result = [];
        for($i = 0; $i < count($array); $i++) {
            if (isset($this->indexes[$i])) {
                $result[$this->indexes[$i]] = $array[$i];
            } else {
                $result[$i] = $array[$i];
            }
        }
        return $result;
    }

}
