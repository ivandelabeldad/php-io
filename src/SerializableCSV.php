<?php


namespace IO;


interface SerializableCSV
{

    public function serializeCSV();

    public function unserializeCSV();

}
