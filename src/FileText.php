<?php

namespace IO;

class FileText extends File
{
    /**
     * @var string
     */
    protected $endOfLine;

    /**
     * FileText constructor.
     * @param string $fileName
     * @param string $endOfLine
     */
    public function __construct($fileName, $endOfLine = "\n")
    {
        parent::__construct($fileName);
        $this->endOfLine = $endOfLine;
    }

    /**
     * @return string
     */
    public function readContent()
    {
        $file = fopen($this->fileName, 'r');
        $content = '';
        while (!feof($file)) {
            $content .= $this->readLineClean($file);
        }
        fclose($file);
        return $content;
    }

    /**
     * @return string
     */
    public function readFirstLine()
    {
        $content = '';
        $file = fopen($this->fileName, 'r');
        if (!feof($file)) {
            $content = $this->readLineClean($file);
        }
        fclose($file);
        return $content;
    }

    /**
     * @return string
     */
    public function readLastLine()
    {
        $file = fopen($this->fileName, 'r');
        $content = '';
        while (!feof($file)) {
            $tmp = $this->readLineClean($file);
            if ($tmp !== $this->endOfLine) {
                $content = $tmp;
            }
        }
        fclose($file);
        return $content;
    }

    /**
     * @param  $search
     * @param string $replace
     * @param int $limit
     */
    public function replaceFromFile($search, $replace, $limit = -1)
    {
        $content = $this->readContent();
        $content = preg_replace("/".$search."/", $replace, $content, $limit);
        $content = trim($content);
        $this->delete();
        $this->writeLine($content);
    }

    /**
     * @param string $line
     */
    public function writeLine($line)
    {
        $this->createIfNotExists();
        $file = fopen($this->fileName, 'a+');
        fwrite($file, $line . $this->endOfLine);
        fclose($file);
    }

    /**
     * @return FileTextIterator
     */
    public function iterator()
    {
        return new FileTextIterator($this->fileName);
    }

    /**
     * @return string[]
     */
    public function toArray()
    {
        $array = [];
        foreach ($this->iterator() as $item) {
            $array[] = $item;
        }
        return $array;
    }

    /**
     * @param $file
     * @return string
     */
    private function readLineClean($file)
    {
        return trim(fgets($file)) . $this->endOfLine;
    }

}
